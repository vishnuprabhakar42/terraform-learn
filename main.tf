provider "aws" {}

variable "subnet_cidr_block" {
  description = "subnet cidr block"
}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
}

variable "enviroment" {
  description = "env"
}

resource "aws_vpc" "development-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
      Name: var.enviroment
      vpc_env: "dev"
    }

}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = "ap-south-1a"
  tags = {
    Name: "dec-subnet-1"
  }
  
}

# data "aws_vpc" "exesting_vpc" {
#   default = true
  
# } 

# resource "aws_subnet" "dev-subnet-2" {
#   vpc_id = data.aws_vpc.exesting_vpc.id
#   cidr_block = var.subnet_cidr_block
#   availability_zone = "ap-south-1a"
#   tags = {
#     Name: "dev-subnet-2"
#   }


# }